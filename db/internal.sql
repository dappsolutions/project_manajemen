-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 06, 2019 at 03:49 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `internal`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `nama_bank` varchar(150) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `nama_bank`, `company`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'BNI', 1, '2019-06-19', 1, NULL, NULL, 0),
(2, 'BRI', 1, '2019-06-19', 1, NULL, NULL, 0),
(3, 'Mandiri', 1, '2019-06-19', 1, NULL, NULL, 0),
(4, 'BCA', 1, '2019-07-05', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bank_account`
--

CREATE TABLE `bank_account` (
  `id` int(11) NOT NULL,
  `bank` int(11) NOT NULL,
  `no_rek` varchar(255) DEFAULT NULL,
  `nama_account` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_account`
--

INSERT INTO `bank_account` (`id`, `bank`, `no_rek`, `nama_account`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(4, 3, '171-000-442-654-3', 'Hizbatul Ikrima', '2019-06-19', 1, '2019-06-23 05:09:20', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `alamat` text,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `bank_account` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `nama`, `alamat`, `email`, `no_hp`, `user`, `bank_account`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'DAPPSOLUTIONS', 'Surabaya, Jawa Timur, Indonesia', 'solutionsdapps@gmail.com', '+6281748233712', 7, 4, '2019-06-19', 1, '2019-07-05 04:17:15', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama`, `alamat`, `email`, `no_hp`, `user`, `company`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'PT Maspin', 'Malang', 'maspin@gmail.com', '+628594733432', 2, NULL, '2019-06-19', 1, '2019-06-28 08:03:17', 1, 1),
(2, 'PT PLN Transmisi Jawa Timur dan Bali', 'Surabaya, Jawa Timur, Indonesia', 'kwijayanthy79@gmail.com', '+6281331131133', 4, 1, '2019-06-23', 1, NULL, NULL, 0),
(3, 'PT PLN Transmisi Jawa Timur dan Bali', 'Surabaya', 'adi85laksono@gmail.com', '+628175120567', 5, 1, '2019-06-28', 1, '2019-06-28 08:04:06', 1, 0),
(4, 'Loakin GO', 'Malang', 'loakingo@gmail.com', '+6281748233712', 8, 1, '2019-07-05', 7, NULL, NULL, 0),
(5, 'Trayek', 'Malang ITN', '-', '-', 9, 1, '2019-08-10', 7, NULL, NULL, 0),
(6, 'Gedung', 'MALANG ITN', '-', '-', 10, 1, '2019-08-10', 7, NULL, NULL, 0),
(7, 'SD', 'MALANG ITN', '-', '-', 11, 1, '2019-08-10', 7, NULL, NULL, 0),
(8, 'PT PLN Transmisi Jawa Timur dan Bali', 'Surabaya', 'dendiuptmlg2@gmail.com', '+6285234420015', 12, 1, '2019-08-10', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_fitur`
--

CREATE TABLE `detail_fitur` (
  `id` int(11) NOT NULL,
  `fitur` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_fitur`
--

INSERT INTO `detail_fitur` (`id`, `fitur`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'Fitur dd', '2019-06-15', 1, NULL, NULL, 0),
(2, 2, 'Detail DD', '2019-06-15', 1, NULL, NULL, 0),
(3, 3, 'Fitur cc', '2019-06-15', 1, NULL, NULL, 0),
(4, 3, 'Fitur SSS', '2019-06-15', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fitur`
--

CREATE TABLE `fitur` (
  `id` int(11) NOT NULL,
  `produk` int(11) NOT NULL,
  `fitur` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fitur`
--

INSERT INTO `fitur` (`id`, `produk`, `fitur`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 1, 'Fitur A', '2019-06-15', 1, NULL, NULL, 0),
(3, 1, 'Fitur B', '2019-06-15', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_project`
--

CREATE TABLE `kategori_project` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_project`
--

INSERT INTO `kategori_project` (`id`, `kategori`, `company`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Aplikasi', 1, '2019-06-15', 1, '2019-06-15 06:30:13', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `no_pembayaran` varchar(150) DEFAULT NULL,
  `project` int(11) NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `no_pembayaran`, `project`, `tanggal_bayar`, `keterangan`, `total_bayar`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'TRANS19JUN001', 2, '2019-06-12', 'Dp Aplikasi', 3000000, '2019-06-17', 1, '2019-06-23 06:01:07', 1, 1),
(2, 'TRANS19JUN002', 3, '2019-06-25', 'Pembayaran Angsuran ke dua', 1600000, '2019-06-23', 1, '2019-06-23 06:00:55', 1, 1),
(3, 'TRANS19JUN003', 3, '2019-04-16', 'Dp Aplikasi Probis', 3200000, '2019-06-23', 1, NULL, NULL, 0),
(4, 'TRANS19JUN004', 3, '2019-05-24', 'Angsuran Pertama', 1600000, '2019-06-23', 1, NULL, NULL, 0),
(5, 'TRANS19JUN005', 3, '2019-06-25', 'Angsuran Ke dua', 1600000, '2019-06-23', 1, NULL, NULL, 0),
(6, 'TRANS19JUN006', 4, '2019-05-15', 'Dp Aplikasi', 2640000, '2019-06-28', 1, NULL, NULL, 0),
(7, 'TRANS19JUN007', 4, '2019-07-01', 'Angsuran Pertama', 3872000, '2019-06-28', 1, NULL, NULL, 0),
(8, 'TRANS19JUL001', 3, '2019-05-15', 'ANGUSRAN\n', 1600000, '2019-07-05', 7, '2019-07-05 09:06:52', 7, 1),
(9, 'TRANS19JUL002', 5, '2019-05-15', 'Dp Aplikasi', 2000000, '2019-07-05', 7, NULL, NULL, 0),
(10, 'TRANS19JUL003', 5, '2019-06-10', 'Angsuran Pertama', 1500000, '2019-07-05', 7, NULL, NULL, 0),
(11, 'TRANS19JUL004', 3, '2019-07-25', 'Pembayaran Tahap Akhir', 1600000, '2019-07-24', 7, NULL, NULL, 0),
(12, 'TRANS19JUL005', 4, '2019-08-01', 'PEMBAYARAN ANGURAN KEDUA', 3872000, '2019-07-31', 7, NULL, NULL, 0),
(13, 'TRANS19AUG001', 6, '2019-05-31', 'DP', 500000, '2019-08-10', 7, NULL, NULL, 0),
(14, 'TRANS19AUG002', 6, '2019-08-09', 'Pelunasan', 1500000, '2019-08-10', 7, NULL, NULL, 0),
(15, 'TRANS19AUG003', 3, '2019-08-16', 'DP Aplikasi', 4000000, '2019-08-10', 7, '2019-08-16 10:50:02', 7, 0),
(16, 'TRANS19AUG004', 3, '2019-08-16', 'DP Aplikasi Manajemen Data', 4000000, '2019-08-16', 7, NULL, NULL, 0),
(17, 'TRANS19AUG005', 9, '2019-08-16', 'Dp Aplikasi Manajemen Data', 4000000, '2019-08-16', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'superadmin'),
(2, 'customer'),
(3, 'company');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(45) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama_produk`, `harga`, `company`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'SIPINTAR', 12000000, 1, '2019-06-15', 1, NULL, NULL, 0),
(2, 'Aplikasi Probis', 8000000, 1, '2019-06-23', 1, NULL, NULL, 0),
(3, 'Aplikasi Anomali dan Inventori', 22000000, 1, '2019-06-28', 1, NULL, NULL, 0),
(4, 'Aplikasi Market Loak', 5000000, 1, '2019-07-05', 7, NULL, NULL, 0),
(5, 'Aplikasi Trayek Angkot', 2000000, 1, '2019-08-10', 7, NULL, NULL, 0),
(6, 'Aplikasi Persebaran SD', 1500000, 1, '2019-08-10', 7, NULL, NULL, 0),
(7, 'Aplikasi Gedung', 1500000, 1, '2019-08-10', 7, NULL, NULL, 0),
(8, 'Aplikasi Manajemen Data', 10000000, 1, '2019-08-10', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `no_project` varchar(100) DEFAULT NULL,
  `customer` int(11) DEFAULT NULL,
  `nama_project` varchar(150) DEFAULT NULL,
  `tanggal_order` date DEFAULT NULL,
  `kategori_project` int(11) NOT NULL,
  `produk` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `no_project`, `customer`, `nama_project`, `tanggal_order`, `kategori_project`, `produk`, `harga`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'PROJECT19JUN002', 1, 'Aplikasi Ujian Online SMK', '2019-06-15', 1, 1, 13000000, '2019-06-15', 1, '2019-06-28 08:03:28', 1, 1),
(3, 'PROJECT19JUN003', 2, 'Aplikasi Manajamen Probis', '2019-04-16', 1, 2, 8000000, '2019-06-23', 1, NULL, NULL, 0),
(4, 'PROJECT19JUN004', 3, 'Aplikasi Peminjaman Alat dan Anomali Alat', '2019-05-15', 1, 3, 22000000, '2019-06-28', 1, NULL, NULL, 0),
(5, 'PROJECT19JUL001', 4, 'Loakin Go Apps', '2019-05-15', 1, 4, 5000000, '2019-07-05', 7, NULL, NULL, 0),
(6, 'PROJECT19AUG002', 5, 'Skripsi Trayek Angkot Android', '2019-06-01', 1, 5, 2000000, '2019-08-10', 7, '2019-08-10 07:28:27', 7, 0),
(7, 'PROJECT19AUG003', 6, 'Skripsi Gedung Android', '2019-06-01', 1, 7, 1500000, '2019-08-10', 7, NULL, NULL, 0),
(8, 'PROJECT19AUG004', 7, 'Skripsi Persebaran SD Android', '2019-06-01', 1, 6, 1500000, '2019-08-10', 7, NULL, NULL, 0),
(9, 'PROJECT19AUG005', 8, 'Aplikasi Manajemen Data', '2019-08-07', 1, 8, 10000000, '2019-08-10', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_progress`
--

CREATE TABLE `project_progress` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_progress`
--

INSERT INTO `project_progress` (`id`, `project`, `tanggal`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, '2019-06-11', 'Masih Progress 70%', '2019-06-19', 1, NULL, NULL, 0),
(2, 3, '2019-05-05', 'Aplikasi Selesai Development 100%', '2019-06-23', 1, NULL, NULL, 0),
(3, 4, '2019-07-05', 'Aplikasi Android dan Web Peminjaman Sudah OK', '2019-07-05', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_status`
--

CREATE TABLE `project_status` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nON GOING\nCOMPLETE',
  `keterangan` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_status`
--

INSERT INTO `project_status` (`id`, `project`, `status`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'DRAFT', NULL, '2019-06-15', 1, NULL, NULL, 0),
(2, 3, 'DRAFT', NULL, '2019-06-23', 1, NULL, NULL, 0),
(3, 3, 'COMPLETE', NULL, '2019-06-23', 1, NULL, NULL, 0),
(4, 4, 'DRAFT', NULL, '2019-06-28', 1, NULL, NULL, 0),
(5, 5, 'DRAFT', NULL, '2019-07-05', 7, NULL, NULL, 0),
(6, 5, 'COMPLETE', NULL, '2019-08-10', 7, NULL, NULL, 0),
(7, 6, 'DRAFT', NULL, '2019-08-10', 7, NULL, NULL, 0),
(8, 7, 'DRAFT', NULL, '2019-08-10', 7, NULL, NULL, 0),
(9, 8, 'DRAFT', NULL, '2019-08-10', 7, NULL, NULL, 0),
(10, 6, 'COMPLETE', NULL, '2019-08-10', 7, NULL, NULL, 0),
(11, 9, 'DRAFT', NULL, '2019-08-10', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id` int(11) NOT NULL,
  `pembayaran` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'LUNAS,\nPENDING',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_pembayaran`
--

INSERT INTO `status_pembayaran` (`id`, `pembayaran`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'PENDING', '2019-06-17', 1, NULL, NULL, 0),
(2, 1, 'LUNAS', '2019-06-17', 1, NULL, NULL, 0),
(3, 2, 'PENDING', '2019-06-23', 1, NULL, NULL, 0),
(4, 3, 'PENDING', '2019-06-23', 1, NULL, NULL, 0),
(5, 3, 'LUNAS', '2019-06-23', 1, NULL, NULL, 0),
(6, 4, 'PENDING', '2019-06-23', 1, NULL, NULL, 0),
(7, 4, 'LUNAS', '2019-06-23', 1, NULL, NULL, 0),
(8, 5, 'PENDING', '2019-06-23', 1, NULL, NULL, 0),
(9, 5, 'LUNAS', '2019-06-25', 1, NULL, NULL, 0),
(10, 6, 'PENDING', '2019-06-28', 1, NULL, NULL, 0),
(11, 6, 'LUNAS', '2019-06-28', 1, NULL, NULL, 0),
(12, 7, 'PENDING', '2019-06-28', 1, NULL, NULL, 0),
(13, 7, 'LUNAS', '2019-07-05', 1, NULL, NULL, 0),
(14, 8, 'PENDING', '2019-07-05', 7, NULL, NULL, 0),
(15, 8, 'LUNAS', '2019-07-05', 7, NULL, NULL, 0),
(16, 9, 'PENDING', '2019-07-05', 7, NULL, NULL, 0),
(17, 9, 'LUNAS', '2019-07-05', 7, NULL, NULL, 0),
(18, 10, 'PENDING', '2019-07-05', 7, NULL, NULL, 0),
(19, 10, 'LUNAS', '2019-07-05', 7, NULL, NULL, 0),
(20, 11, 'PENDING', '2019-07-24', 7, NULL, NULL, 0),
(21, 11, 'LUNAS', '2019-07-25', 7, NULL, NULL, 0),
(22, 12, 'PENDING', '2019-07-31', 7, NULL, NULL, 0),
(23, 12, 'LUNAS', '2019-08-10', 7, NULL, NULL, 0),
(24, 13, 'PENDING', '2019-08-10', 7, NULL, NULL, 0),
(25, 13, 'LUNAS', '2019-08-10', 7, NULL, NULL, 0),
(26, 14, 'PENDING', '2019-08-10', 7, NULL, NULL, 0),
(27, 14, 'LUNAS', '2019-08-10', 7, NULL, NULL, 0),
(28, 15, 'PENDING', '2019-08-10', 7, NULL, NULL, 0),
(29, 16, 'PENDING', '2019-08-16', 7, NULL, NULL, 0),
(30, 17, 'PENDING', '2019-08-16', 7, NULL, NULL, 0),
(31, 17, 'LUNAS', '2019-08-20', 7, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `priveledge` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'admin', 'admin', 1, NULL, NULL, NULL, NULL, 0),
(2, 'maspin@gmail.com', 'maspin@gmail.com', 2, '2019-06-19', 1, NULL, NULL, 0),
(3, 'greenhole@gmail.com', 'greenhole@gmail.com', 3, '2019-06-19', 1, NULL, NULL, 0),
(4, 'kwijayanthy79@gmail.com', 'kwijayanthy79@gmail.com', 2, '2019-06-23', 1, NULL, NULL, 0),
(5, 'adi85laksono@gmail.com', 'adi85laksono@gmail.com', 2, '2019-06-28', 1, NULL, NULL, 0),
(7, 'dapps', 'sasuke', 3, '2019-07-05', 1, '2019-07-05 04:17:15', 1, 0),
(8, 'loakingo@gmail.com', 'loakingo@gmail.com', 2, '2019-07-05', 7, NULL, NULL, 0),
(9, '-', '-', 2, '2019-08-10', 7, NULL, NULL, 0),
(10, '-', '-', 2, '2019-08-10', 7, NULL, NULL, 0),
(11, '-', '-', 2, '2019-08-10', 7, NULL, NULL, 0),
(12, 'dendiuptmlg2@gmail.com', 'dendiuptmlg2@gmail.com', 2, '2019-08-10', 7, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_account`
--
ALTER TABLE `bank_account`
  ADD PRIMARY KEY (`id`,`bank`),
  ADD KEY `fk_bank_account_bank1_idx` (`bank`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`,`bank_account`),
  ADD KEY `fk_company_bank_account1_idx` (`bank_account`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_fitur`
--
ALTER TABLE `detail_fitur`
  ADD PRIMARY KEY (`id`,`fitur`);

--
-- Indexes for table `fitur`
--
ALTER TABLE `fitur`
  ADD PRIMARY KEY (`id`,`produk`),
  ADD KEY `fk_fitur_produk1_idx` (`produk`);

--
-- Indexes for table `kategori_project`
--
ALTER TABLE `kategori_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`,`project`),
  ADD KEY `fk_pembayaran_project1_idx` (`project`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`,`kategori_project`,`produk`);

--
-- Indexes for table `project_progress`
--
ALTER TABLE `project_progress`
  ADD PRIMARY KEY (`id`,`project`),
  ADD KEY `fk_project_progress_project1_idx` (`project`);

--
-- Indexes for table `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`id`,`project`),
  ADD KEY `fk_project_status_project_idx` (`project`);

--
-- Indexes for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id`,`pembayaran`),
  ADD KEY `fk_status_pembayaran_pembayaran1_idx` (`pembayaran`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bank_account`
--
ALTER TABLE `bank_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detail_fitur`
--
ALTER TABLE `detail_fitur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fitur`
--
ALTER TABLE `fitur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kategori_project`
--
ALTER TABLE `kategori_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `project_progress`
--
ALTER TABLE `project_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_status`
--
ALTER TABLE `project_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

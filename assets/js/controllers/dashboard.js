var Dashboard = {
 module: function () {
  return 'dashboard';
 },

 randomData: function () {
  barChartData.datasets.forEach(function (dataset) {
   dataset.data = dataset.data.map(function () {
    return randomScalingFactor();
   });
  });
  window.myBar.update();
 },

 getDataJamaah: function () {
  var data_kehadiran = $('#data_kehadiran').val();
  var kehadiran = data_kehadiran.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(kehadiran[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

  //  console.log(data_all);
  return data_all;
 },

 getDataJamaahMasjid: function () {
  var data_kehadiran = $('#data_kehadiran_masjid').val();
  var kehadiran = data_kehadiran.toString().split(',');
  var data_all = [];
  for (var i = 0; i < data_kehadiran.length; i++) {
   var value = parseFloat(kehadiran[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

  //  console.log(data_all);
  return data_all;
 },

 getDataKajian: function () {
  var data_kajian = $('#data_kajian').val();

  var kajian = data_kajian.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(kajian[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataJumat: function () {
  var data_jumat = $('#data_jumat').val();

  var jumat = data_jumat.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(jumat[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataKehadiran: function () {
  var data_hadir = $('#data_hadir').val();

  var kajian = data_hadir.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(kajian[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataJumlahMasjid: function () {
  var data_jumlah_masjid = $('#data_jumlah_masjid').val();

  var jumlah = data_jumlah_masjid.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(jumlah[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataJumlahOrganisasi: function () {
  var data_jumlah_org = $('#data_jumlah_org').val();

  var jumlah = data_jumlah_org.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(jumlah[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataPembelianResep: function () {
  // var data_pembelian = $('#data_pembelian_resep').val();
  //  var pembelian = data_pembelian.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   // var value = parseFloat(pembelian[i]).toFixed(2);
   var value = i * 30;
   data_all.push(value);
  }

  console.log(data_all);
  return data_all;
 },

 getDataLabelJamaah: function () {
  var data = [];
  data.push("Magrib");
  data.push("Isya'");
  data.push("Subuh");
  data.push("Dzuhur");
  data.push("Ashar");

  return data;
 },

 getDataLabelJamaahMasjid: function () {
  var data = [];
  var data_masjid = $('input#data_masjid').val();
//   console.log("dataMasjid",data_masjid);
  var masjid = data_masjid.toString().split(',');
  for (var i = 0; i < masjid.length; i++) {
   var value = masjid[i];
   // var value = i * 30;
   data.push(value);
  }

//  console.log('data masjid', data);

  return data;
 },

 getDataLabelKajian: function () {
  var data = [];
  for (i = 1; i < 32; i++) {
   data.push(i);
  }


  return data;
 },

 setGrafikJamaah: function () {
  var data_sholat = $('input#data_jamaah_sholat').val();
  console.log(data_sholat[0]);
//  return;
  Morris.Bar({
   element: 'canvas_jamaah',
   data: data_sholat,
   xkey: 'sholat',
   ykeys: ['jumlah'],
   labels: ['Sholat']
  });
 },

 setGrafikJamaahMasjid: function () {
  var ctx = document.getElementById('canvas_jamaah_masjid').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelJamaahMasjid(),
   datasets: [{
     label: 'Kehadiran Jamaah Masjid',
     backgroundColor: window.chartColors.purple,
     yAxisID: 'kehadiran_masjid',
     data: Dashboard.getDataJamaahMasjid()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Kehadiran Jamaah Masjid'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'kehadiran_masjid',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_kehadiran_masjid').val())
       }
      }]
    }
   }
  });
 },

 setGrafikKajian: function () {
  var ctx = document.getElementById('canvas_kajian').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelKajian(),
   datasets: [{
     label: 'Kehadiran Jamaah Kajian',
     backgroundColor: window.chartColors.red,
     yAxisID: 'kajian',
     data: Dashboard.getDataKajian()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Kehadiran Jamaah Kajian'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'kajian',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_kajian').val())
       }
      }]
    }
   }
  });
 },

 setGrafikJumat: function () {
  var ctx = document.getElementById('canvas_jumat').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelKajian(),
   datasets: [{
     label: 'Kehadiran Jamaah Sholat Jumat',
     backgroundColor: window.chartColors.green,
     yAxisID: 'jumat',
     data: Dashboard.getDataJumat()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Kehadiran Jamaah Sholat Jumat'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'jumat',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_jumat').val())
       }
      }]
    }
   }
  });
 },

 setGrafikKehadiran: function () {
  var ctx = document.getElementById('canvas_kajian').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelKajian(),
   datasets: [{
     label: 'Kehadiran Jamaah',
     backgroundColor: window.chartColors.green,
     yAxisID: 'hadir',
     data: Dashboard.getDataKehadiran()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Kehadiran Jamaah'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'hadir',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_hadir').val())
       }
      }]
    }
   }
  });
 },

 setGrafikDataMasjid: function () {
  var ctx = document.getElementById('canvas_jumlah_masjid').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelKajian(),
   datasets: [{
     label: 'Data Jumlah Masjid',
     backgroundColor: window.chartColors.green,
     yAxisID: 'jumlah_masjid',
     data: Dashboard.getDataJumlahMasjid()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Data Jumlah Masjid'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'jumlah_masjid',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_jumlah_masjid').val())
       }
      }]
    }
   }
  });
 },

 setGrafikDataOrganisasi: function () {
  var ctx = document.getElementById('canvas_jumlah_org').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelKajian(),
   datasets: [{
     label: 'Data Jumlah Organisasi',
     backgroundColor: window.chartColors.orange,
     yAxisID: 'organisasi',
     data: Dashboard.getDataJumlahOrganisasi()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Data Jumlah Organisasi'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'organisasi',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_jumlah_org').val())
       }
      }]
    }
   }
  });
 },

 setMapMasjid: function () {
  var map = document.getElementById("map");
  var mp = $('div#map');
  console.log('mapo', mp);
 },

 setDateRangePicker: function () {
  console.log('ok date');
  $('input#date_dashboard').daterangepicker();
 },

 showPeraturanProbis: function () {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showRuleProbis",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 gotoProbis: function () {
  window.location.href = url.base_url("probis") + "index";
 },

 gotoReferensi: function () {
  window.location.href = url.base_url("referensi_probis") + "index";
 },

 infoProbis: function () {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showInfoProbis",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 showDetailPeraturanProbis: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showDetailRuleProbis/" + id,
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
};


$(function () {

});

var Produk = {
 module: function () {
  return 'produk';
 },

 add: function () {
  window.location.href = url.base_url(Produk.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Produk.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Produk.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Produk.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama_produk': $('#nama_produk').val(),
   'harga': $('#harga').val(),
  };

  return data;
 },

 getPostDataFitur: function () {
  var data = [];
  var tb = $('table#tb_fitur').find('tbody').find('tr');
  $.each(tb, function () {
   data.push({
    'keterangan': $(this).find('input#ket').val()
   });
  });

  return data;
 },

 simpanFitur: function () {
  var produk_id = $('input#id').val();

  var data = Produk.getPostDataFitur();
  var formData = new FormData();
  formData.append("data", JSON.stringify(data));
  formData.append("nama_fitur", $('input#nama_fitur').val())

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Produk.module()) + "simpanFitur/" + produk_id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Produk.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Produk.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Produk.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Produk.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Produk.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h4>Apa anda yakin akan menghapus data ini ?</h4>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10' onclick='Produk.execDeleted(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Produk.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Produk.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 upload: function (elm) {
  var input = $(elm).closest('span').find('input');
  input.click();
 },

 getFilename: function (elm, type) {
  Produk.checkFile(elm, type);
 },

 checkFile: function (elm, type) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == type) {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Produk.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 previewFile: function (name_of_file) {
  $.ajax({
   type: 'POST',
   data: {
    file: name_of_file
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Produk.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Produk.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Produk.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Produk.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#issue_date').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#due_date').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#remember_date').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 setDataSelect: function () {
  $("#probis").select2();
 },

 chooseProbis: function (elm) {
  var tr = $(elm).closest('tr');

  var option = $(elm).find('option');
  var no_dokumen = "";
  $.each(option, function () {
   if ($(this).is(':selected')) {
    no_dokumen = $(this).attr('no_dokumen');
//    console.log('keyword',$(this).find('label#keyword'));
   }
  });

  tr.find('input#no_dokumen').val(no_dokumen);
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var tr_index = tr.index();
  var newTr = tr.clone();

  var new_tr_index = tr_index + 1;
  newTr.find('input').val('');

  var html = '';
  html += ' <i class="mdi mdi-minus-circle mdi-24px hover text-danger"';
  html += 'onclick="Produk.removeDetail(this)"></i>';
  newTr.find('td:eq(1)').html(html);
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },
};

$(function () {
 Produk.setDate();
});
<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateNoProject() {
  $no = 'PROJECT' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'project',
  'like' => array(
  array('no_project', $no)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['no_project']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }
 
 public function generateNoPembayaran() {
  $no = 'TRANS' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'pembayaran',
  'like' => array(
  array('no_pembayaran', $no)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['no_pembayaran']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }

}

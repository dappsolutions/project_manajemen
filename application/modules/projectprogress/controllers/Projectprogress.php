<?php

class Projectprogress extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $hak_akses;
 public $company_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'projectprogress';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/projectprogress.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'project_progress';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Project Progress";
  $data['title_content'] = 'Data Project Progress';
  $content = $this->getDataProjectprogress();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProjectprogress($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_project', $keyword),
       array('p.nama_project', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pp.deleted = 0";
    break;
   case "company":
    $where = "pp.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "pp.deleted = 0 and p.customer = '" . $this->session->userdata('customer_id') . "'";
    break;
   default:
    break;
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' pp',
              'field' => array('pp.*', 'p.no_project', 'p.nama_project'),
              'join' => array(
                  array('project p', 'pp.project = p.id'),
                  array('kategori_project kp', 'kp.id = p.kategori_project'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "pp.deleted = 0"
  ));

  return $total;
 }

 public function getDataProjectprogress($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_project', $keyword),
       array('p.nama_project', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pp.deleted = 0";
    break;
   case "company":
    $where = "pp.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "pp.deleted = 0 and p.customer = '" . $this->session->userdata('customer_id') . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pp',
              'field' => array('pp.*', 'p.no_project', 'p.nama_project'),
              'join' => array(
                  array('project p', 'pp.project = p.id'),
                  array('kategori_project kp', 'kp.id = p.kategori_project'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProjectprogress($keyword)
  );
 }

 public function getListProject() {
  $data = Modules::run('database/get', array(
              'table' => 'project p',
              'join' => array(
                  array('produk pp', 'p.produk = pp.id')
              ),
              'where' => "p.deleted = 0 and pp.company = '" . $this->company_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Project Progress";
  $data['title_content'] = 'Tambah Project Progress';
  $data['list_project'] = $this->getListProject();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProjectprogress($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Project Progress";
  $data['title_content'] = 'Ubah Project Progress';
  $data['list_project'] = $this->getListProject();
  echo Modules::run('template', $data);
 }

 public function getDetailDataProjectprogress($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pp',
              'field' => array('pp.*', 'p.no_project', 'p.nama_project'),
              'join' => array(
                  array('project p', 'pp.project = p.id')
              ),
              'where' => "pp.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataProjectprogress($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Project Progress";
  $data['title_content'] = "Detail Project Progress";
  echo Modules::run('template', $data);
 }

 public function getPostData($value) {
  $data['project'] = $value->project;
  $data['tanggal'] = $value->tanggal;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Projectprogress";
  $data['title_content'] = 'Data Projectprogress';
  $content = $this->getDataProjectprogress($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

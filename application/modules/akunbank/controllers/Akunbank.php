<?php

class Akunbank extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $hak_akses;
 public $company_id;


 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'akunbank';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/akunbank.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'bank_account';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Akun bank";
  $data['title_content'] = 'Data Akun bank';
  $content = $this->getDataAkunbank();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAkunbank($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('ba.nama_account', $keyword),
       array('ba.no_rek', $keyword),
       array('b.nama_bank', $keyword)
   );
  }
  switch ($this->hak_akses) {
   case "superadmin":
    $where = "ba.deleted = 0";
    break;
   case "company":
    $where = "ba.deleted = 0 and b.company = '" . $this->company_id . "'";
    break;

   default:
    break;
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' ba',
              'field' => array('ba.*', 'b.nama_bank'),
              'join' => array(
                  array('bank b', 'ba.bank = b.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "ba.deleted = 0"
  ));

  return $total;
 }

 public function getDataAkunbank($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('ba.nama_account', $keyword),
       array('ba.no_rek', $keyword),
       array('b.nama_bank', $keyword)
   );
  }
  
  switch ($this->hak_akses) {
   case "superadmin":
    $where = "ba.deleted = 0";
    break;
   case "company":
    $where = "ba.deleted = 0 and b.company = '" . $this->company_id . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' ba',
              'field' => array('ba.*', 'b.nama_bank'),
              'join' => array(
                  array('bank b', 'ba.bank = b.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAkunbank($keyword)
  );
 }

 public function getListBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank b',
              'where' => "b.deleted = 0 and b.company = '".$this->company_id."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Akun bank";
  $data['title_content'] = 'Tambah Akun bank';
  $data['list_bank'] = $this->getListBank();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataAkunbank($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Akun bank";
  $data['title_content'] = 'Ubah Akun bank';
  $data['list_bank'] = $this->getListBank();
  echo Modules::run('template', $data);
 }

 public function getDetailDataAkunbank($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field'=> array('k.*', 'b.nama_bank'),
              'join'=> array(
                  array('bank b', 'k.bank = b.id')
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataAkunbank($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Akun bank";
  $data['title_content'] = "Detail Akun bank";
  echo Modules::run('template', $data);
 }

 public function getPostData($value) {
  $data['nama_account'] = $value->nama_account;
  $data['no_rek'] = $value->no_rek;
  $data['bank'] = $value->bank;
  return $data;
 }

 public function getPostAkunbank($value) {
  $data['validity'] = $value->validity;
  $data['issue_date'] = $value->issue_date;
  $data['due_date'] = $value->due_date;
  $data['remember_date'] = $value->remember_date;
  $data['conducted_by'] = $value->conducted_by;
  $data['status'] = $value->status;
  $data['manufacture'] = $value->manufacture;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Akunbank";
  $data['title_content'] = 'Data Akunbank';
  $content = $this->getDataAkunbank($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

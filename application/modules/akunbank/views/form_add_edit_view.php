<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Akun bank <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama akun</label>
           <div class="col-md-9">
            <input type="text" id='nama_account' class="form-control required" error='Nama Akun bank' placeholder="Nama Akun bank" value="<?php echo isset($nama_account) ? $nama_account : '' ?>">
           </div>
          </div>
         </div>

         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">No Rekening</label>
           <div class="col-md-9">
            <input type="text" id='no_rek' class="form-control required" error='No Rekening' placeholder="No Rekening" value="<?php echo isset($no_rek) ? $no_rek : '' ?>">
           </div>
          </div>
         </div>
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Bank</label>
           <div class="col-md-9">
            <select id='bank' class="form-control required" error='Bank'>
             <?php if (!empty($list_bank)) { ?>
              <?php foreach ($list_bank as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($bank)) { ?>
                <?php $selected = $bank == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_bank'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>       
       </div>

       <div class="row">
        <div class="col-md-12">
         <div class="row">
          <div class="col-md-offset-3 col-md-9 text-right">
           <button type="submit" class="btn btn-success" onclick="Akunbank.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
           <button type="button" class="btn btn-default" onclick="Akunbank.back()">Batal</button>
          </div>
         </div>
        </div>
        <div class="col-md-6"> </div>
       </div>
     </div>

     <br/>
     </form>
    </div>
   </div>
  </div>
 </div> 
</div>
<!--./row--> 
</div>

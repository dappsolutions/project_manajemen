<?php

class Company extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'company';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/company.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'company';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Perusahaan";
  $data['title_content'] = 'Data Perusahaan';
  $content = $this->getDataCompany();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataCompany($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('c.nama', $keyword),
       array('c.alamat', $keyword),
       array('c.email', $keyword),
       array('c.no_hp', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' c',
              'field' => array('c.*', 'ba.nama_account', 'ba.no_rek', 'b.nama_bank'),
              'join' => array(
                  array('bank_account ba', 'c.bank_account = ba.id', 'left'),
                  array('bank b', 'ba.bank = b.id', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "c.deleted = 0"
  ));

  return $total;
 }

 public function getDataCompany($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('c.nama', $keyword),
       array('c.alamat', $keyword),
       array('c.email', $keyword),
       array('c.no_hp', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' c',
              'field' => array('c.*', 'ba.nama_account', 'ba.no_rek', 'b.nama_bank'),
              'join' => array(
                  array('bank_account ba', 'c.bank_account = ba.id', 'left'),
                  array('bank b', 'ba.bank = b.id', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "c.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataCompany($keyword)
  );
 }

 public function getListAkunBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank_account ba',
              'field' => array('ba.*', 'b.nama_bank'),
              'join' => array(
                  array('bank b', 'ba.bank = b.id')
              ),
              'where' => "ba.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Perusahaan";
  $data['title_content'] = 'Tambah Perusahaan';
  $data['list_akun'] = $this->getListAkunBank();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataCompany($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Perusahaan";
  $data['title_content'] = 'Ubah Perusahaan';
  $data['list_akun'] = $this->getListAkunBank();
  echo Modules::run('template', $data);
 }

 public function getDetailDataCompany($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'ba.nama_account', 'ba.no_rek', 'b.nama_bank',
                  'u.username', 'u.password', 'u.id as id_user'),
              'join' => array(
                  array('bank_account ba', 'k.bank_account = ba.id', 'left'),
                  array('bank b', 'ba.bank = b.id', 'left'),
                  array('user u', 'u.id = k.user', 'left'),
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataCompany($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Perusahaan";
  $data['title_content'] = "Detail Perusahaan";
  echo Modules::run('template', $data);
 }

 public function getPostData($value) {
  $data['nama'] = $value->nama;
  $data['email'] = $value->email;
  $data['no_hp'] = $value->no_hp;
  $data['alamat'] = $value->alamat;
  $data['bank_account'] = $value->akun_bank;
  return $data;
 }

 public function getPostCompany($value) {
  $data['validity'] = $value->validity;
  $data['issue_date'] = $value->issue_date;
  $data['due_date'] = $value->due_date;
  $data['remember_date'] = $value->remember_date;
  $data['conducted_by'] = $value->conducted_by;
  $data['status'] = $value->status;
  $data['manufacture'] = $value->manufacture;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    //insert user company
    $user['username'] = $data->username;
    $user['password'] = $data->password;
    $user['priveledge'] = 3;
    $user_id = Modules::run('database/_insert', "user", $user);

    $post['user'] = $user_id;
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    $user_id = $data->id_user;
    if ($user_id == "") {
     //insert user company
     $user['username'] = $data->username;
     $user['password'] = $data->password;
     $user['priveledge'] = 3;
     $user_id = Modules::run('database/_insert', "user", $user);
    } else {
     Modules::run('database/_update', 'user', array(
         'username'=> $data->username,
         'password'=> $data->password
     ), array('id' => $user_id));
    }

    $post['user'] = $user_id;
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Company";
  $data['title_content'] = 'Data Company';
  $content = $this->getDataCompany($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

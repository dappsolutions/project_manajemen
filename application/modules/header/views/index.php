<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="keywords" content="">
 <meta name="description" content="">
 <meta name="author" content="">
 <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/css/images/favicon.png">
 <title><?php echo $title_content ?></title>
 <!-- ===== Bootstrap CSS ===== -->
 <link href="<?php echo base_url() ?>assets/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
 <link href="<?php echo base_url() ?>assets/plugins/components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
 <link href="<?php echo base_url() ?>assets/plugins/components/morrisjs/morris.css" rel="stylesheet">
 <!-- ===== Plugin CSS ===== -->
<!-- <link href="<?php echo base_url() ?>assets/plugins/components/chartist-js/dist/chartist.min.css" rel="stylesheet">
 <link href="<?php echo base_url() ?>assets/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">-->
 <!-- ===== Animation CSS ===== -->
 <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
 <!-- ===== Custom CSS ===== -->
 <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
 <!-- ===== Color CSS ===== -->
 <link href="<?php echo base_url() ?>assets/css/colors/default.css" id="theme" rel="stylesheet">
 
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css"> 

<link href="<?php echo base_url() ?>assets/plugins/components/custom-select/custom-select.css" rel="stylesheet" type="text/css" /> 
 <link href="<?php echo base_url() ?>assets/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" /> 
 <link href="<?php echo base_url() ?>assets/plugins/components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
 <link href="<?php echo base_url() ?>assets/plugins/components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
 
 <link href="<?php echo base_url() ?>assets/plugins/components/switchery/dist/switchery.min.css" rel="stylesheet" />
</head>



<!-- ==============================
      Required JS Files
  =============================== -->
<!-- ===== jQuery ===== -->
<script src="<?php echo base_url() ?>assets/plugins/components/jquery/dist/jquery.min.js"></script>
<!-- ===== Bootstrap JavaScript ===== -->
<script src="<?php echo base_url() ?>assets/css/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ===== Slimscroll JavaScript ===== -->
<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
<!-- ===== Wave Effects JavaScript ===== -->
<script src="<?php echo base_url() ?>assets/js/waves.js"></script>
<!-- ===== Menu Plugin JavaScript ===== -->
<script src="<?php echo base_url() ?>assets/js/sidebarmenu.js"></script>
<!-- ===== Custom JavaScript ===== -->
<script src="<?php echo base_url() ?>assets/js/custom.js"></script>
<script src="<?php echo base_url() ?>assets/js/jasny-bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/raphael/raphael-min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/morrisjs/morris.js"></script>
<!-- ===== Plugin JS ===== -->
<script src="<?php echo base_url() ?>assets/plugins/components/chartist-js/dist/chartist.min.js"></script>
<!--<script src="<?php echo base_url() ?>assets/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/knob/jquery.knob.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/easypiechart/dist/jquery.easypiechart.min.js"></script>-->
<script src="<?php echo base_url() ?>assets/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/timepicker/bootstrap-timepicker.min.js"></script>
<!--<script src="<?php echo base_url() ?>assets/js/db1.js"></script>-->

<!-- ===== Style Switcher JS ===== -->

<!--<script src="<?php echo base_url() ?>assets/js/Chart.bundle.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/utils.js"></script>-->
<!-- endinject -->

<script src="<?php echo base_url() ?>assets/js/dashboard.js"></script>
<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>
<script src="<?php echo base_url() ?>assets/js/message.js"></script>
<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
<script src="<?php echo base_url() ?>assets/js/controllers/template.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
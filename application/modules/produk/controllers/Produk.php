<?php

class Produk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $hak_akses;
 public $company_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'produk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/produk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'produk';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Produk";
  $data['title_content'] = 'Data Produk';
  $content = $this->getDataProduk();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProduk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pj.nama_produk', $keyword),
   );
  }
  
  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pj.deleted = 0";
    break;
   case "company":
    $where = "pj.deleted = 0 and pj.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "pj.deleted = 0 and pj.company = '" . $this->company_id . "'";
    break;

   default:
    break;
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*'),
              'where' => $where
  ));

  return $total;
 }

 public function getDataProduk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pj.nama_produk', $keyword),
   );
  }
  
//  echo '<pre>';
//  print_r($_SESSION);
//  die;
  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pj.deleted = 0";
    break;
   case "company":
    $where = "pj.deleted = 0 and pj.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "pj.deleted = 0 and pj.company = '" . $this->company_id . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProduk($keyword)
  );
 }

 public function getDetailFitur($produk) {
  $data = Modules::run('database/get', array(
              'table' => 'fitur ft',
              'field' => array('ft.*', 'dt.keterangan'),
              'join' => array(
                  array('detail_fitur dt', 'dt.fitur = ft.id')
              ),
              'where' => "ft.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   $temp = "";
   foreach ($data->result_array() as $value) {
    if ($temp != $value['fitur']) {
     $value['fitur'] = $value['fitur'];
    } else {
     $value['fitur'] = "";
    }
    array_push($result, $value);
    $temp = $value['fitur'];
   }
  }


  return $result;
 }

 public function fitur($id) {
  $data['id'] = $id;
  $data['view_file'] = 'fitur_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Fitur";
  $data['title_content'] = 'Tambah Fitur';
  $data['list_data'] = $this->getDetailFitur($id);
  echo Modules::run('template', $data);
 }

 public function simpanFitur($produk) {
  $data = json_decode($this->input->post('data'));
  $nama_fitur = $this->input->post("nama_fitur");


  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_fitur['produk'] = $produk;
   $post_fitur['fitur'] = $nama_fitur;
   $fitur = Modules::run('database/_insert', "fitur", $post_fitur);

   foreach ($data as $value) {
    $post_detail['fitur'] = $fitur;
    $post_detail['keterangan'] = $value->keterangan;
    Modules::run('database/_insert', "detail_fitur", $post_detail);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Produk";
  $data['title_content'] = 'Tambah Produk';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProduk($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Produk";
  $data['title_content'] = 'Ubah Produk';
  echo Modules::run('template', $data);
 }

 public function getDetailDataProduk($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'where' => "k.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataProduk($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Produk";
  $data['title_content'] = "Detail Produk";
  echo Modules::run('template', $data);
 }

 public function getPostData($value) {
  $data['nama_produk'] = $value->nama_produk;
  $data['harga'] = $value->harga;
  $data['company'] = $this->company_id;
  return $data;
 }

 public function getPostProduk($value) {
  $data['validity'] = $value->validity;
  $data['issue_date'] = $value->issue_date;
  $data['due_date'] = $value->due_date;
  $data['remember_date'] = $value->remember_date;
  $data['conducted_by'] = $value->conducted_by;
  $data['status'] = $value->status;
  $data['manufacture'] = $value->manufacture;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Produk";
  $data['title_content'] = 'Data Produk';
  $content = $this->getDataProduk($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

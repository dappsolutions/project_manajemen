<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"> <?php echo $title_content ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="#" class="form-horizontal">
                            <div class="form-body">
                                <h3 class="box-title">Detail Fitur <i class="fa fa-arrow-down"></i></h3>
                                <hr class="m-t-0 m-b-40">
                                <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                                 <div class="row">
                                     <div class="col-md-6">
                                         <div class="row">
                                             <div class="col-md-3">
                                                 Nama Fitur
                                             </div>
                                             <div class="col-md-9 text-primary text-left">
                                                 <input type="text" value="" id="nama_fitur" placeholder="Fitur" class="form-control required" error="Nama Fitur" />
                                             </div>
                                         </div>         
                                     </div>
                                 </div>
                                 <br/>

                                 <div class="row">
                                     <div class="col-md-12">
                                         <div class="table-responsive">
                                             <table class="table table-bordered" id="tb_fitur">
                                                 <thead>
                                                     <tr class="">
                                                         <th class="font-12">Keterangan</th>
                                                         <th class="text-center font-12">Action</th>
                                                     </tr>
                                                 </thead>
                                                 <tbody>
                                                     <tr>
                                                         <td>
                                                             <input type="text" value="" id="ket" class="form-control" placeholder="Keterangan"/>
                                                         </td>
                                                         <td class="text-center">
                                                             <i class="mdi mdi-plus-circle mdi-24px text-warning hover" onclick="Produk.addDetail(this)"></i>
                                                         </td>
                                                     </tr>
                                                 </tbody>
                                             </table>
                                         </div>
                                     </div>
                                 </div>
                                 <br/>

                                 <div class="row">
                                     <div class="col-md-12 text-right">
                                         <button class="btn btn-success" onclick="Produk.simpanFitur()">Simpan</button>
                                     </div>
                                 </div>
                                <?php } ?>
                            </div>      
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--./row--> 



    <div class="row">  
        <div class="col-md-12">
            <p><b>List Fitur</b></p>
            <div class="table-responsive">
                <table class="table color-bordered-table success-bordered-table">
                    <thead>
                        <tr class="">
                            <th class="font-12">No</th>
                            <th class="font-12">Nama Fitur</th>
                            <th class="font-12">Detail Fitur</th>
                            <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                             <th class="text-center font-12">Action</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($list_data)) { ?>
                         <?php $no = 0; ?>
                         <?php foreach ($list_data as $value) { ?>
                          <tr>
                              <td class='font-12'><?php
                                  if ($value['fitur'] != '') {
                                   $no += 1;
                                   echo $no;
                                  } else {
                                   echo '';
                                  }
                                  ?></td>
                              <td class='font-12'><b><?php echo $value['fitur'] ?></b></td>
                              <td class='font-12'><?php echo $value['keterangan'] ?></td>
                              <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                               <td class="text-center">
                                   <i class="mdi mdi-grease-pencil mdi-18px hover text-warning" onclick="Produk.ubah('<?php echo $value['id'] ?>')"></i>                
                                   &nbsp;
                                   <i class="mdi mdi-delete mdi-18px hover text-danger" onclick="Produk.delete('<?php echo $value['id'] ?>')"></i>
                               </td>
                              <?php } ?>
                          </tr>
                         <?php } ?>
                        <?php } else { ?>
                         <tr>
                             <td class="text-center font-12" colspan="13">Tidak Ada Data Ditemukan</td>
                         </tr>
                        <?php } ?>         
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-offset-3 col-md-9 text-right">
                    <button type="button" class="btn btn-default" onclick="Produk.back()">Kembali</button>
                </div>
            </div>
        </div>
        <div class="col-md-6"> </div>
    </div>
</div>

<?php

class Pembayaran extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $hak_akses;
 public $company_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'pembayaran';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pembayaran.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembayaran';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pembayaran";
  $data['title_content'] = 'Data Pembayaran';
  $content = $this->getDataPembayaran();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPembayaran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_pembayaran', $keyword),
       array('p.no_project', $keyword),
       array('p.nama_project', $keyword),
       array('p.tanggal_bayar', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "p.deleted = 0";
    break;
   case "company":
    $where = "p.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "p.deleted = 0 and pj.customer = '" . $this->session->userdata('customer_id') . "'";
    break;
   default:
    break;
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'sp.status', 'pj.no_project',
                  'pj.nama_project'),
              'join' => array(
                  array('project pj', 'pj.id = p.project'),
                  array('(select max(id) as id, pembayaran from status_pembayaran group by pembayaran) spp', 'spp.pembayaran = p.id'),
                  array('status_pembayaran sp', 'spp.id = sp.id'),
                  array('kategori_project kp', 'kp.id = pj.kategori_project'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where
  ));

  return $total;
 }

 public function getDataPembayaran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_pembayaran', $keyword),
       array('p.no_project', $keyword),
       array('p.nama_project', $keyword),
       array('p.tanggal_bayar', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "p.deleted = 0";
    break;
   case "company":
    $where = "p.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "p.deleted = 0 and pj.customer = '" . $this->session->userdata('customer_id') . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'sp.status', 'pj.no_project',
                  'pj.nama_project', 'pj.harga'),
              'join' => array(
                  array('project pj', 'pj.id = p.project'),
                  array('(select max(id) as id, pembayaran from status_pembayaran group by pembayaran) spp', 'spp.pembayaran = p.id'),
                  array('status_pembayaran sp', 'spp.id = sp.id'),
                  array('kategori_project kp', 'kp.id = pj.kategori_project'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPembayaran($keyword)
  );
 }

 public function getDetailFitur($produk) {
  $data = Modules::run('database/get', array(
              'table' => 'fitur ft',
              'field' => array('ft.*', 'dt.keterangan'),
              'join' => array(
                  array('detail_fitur dt', 'dt.fitur = ft.id')
              ),
              'where' => "ft.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   $temp = "";
   foreach ($data->result_array() as $value) {
    if ($temp != $value['fitur']) {
     $value['fitur'] = $value['fitur'];
    } else {
     $value['fitur'] = "";
    }
    array_push($result, $value);
    $temp = $value['fitur'];
   }
  }


  return $result;
 }

 public function fitur($id) {
  $data['id'] = $id;
  $data['view_file'] = 'fitur_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Fitur";
  $data['title_content'] = 'Tambah Fitur';
  $data['list_data'] = $this->getDetailFitur($id);
  echo Modules::run('template', $data);
 }

 public function simpanFitur($produk) {
  $data = json_decode($this->input->post('data'));
  $nama_fitur = $this->input->post("nama_fitur");


  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_fitur['produk'] = $produk;
   $post_fitur['fitur'] = $nama_fitur;
   $fitur = Modules::run('database/_insert', "fitur", $post_fitur);

   foreach ($data as $value) {
    $post_detail['fitur'] = $fitur;
    $post_detail['keterangan'] = $value->keterangan;
    Modules::run('database/_insert', "detail_fitur", $post_detail);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListProject() {
  $data = Modules::run('database/get', array(
              'table' => 'project p',
              'field' => array('p.*'),
              'join' => array(
                  array('kategori_project kp', 'kp.id = p.kategori_project')
              ),
              'where' => "p.deleted = 0 and kp.company = '" . $this->company_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pembayaran";
  $data['title_content'] = 'Tambah Pembayaran';
  $data['list_project'] = $this->getListProject();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPembayaran($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pembayaran";
  $data['title_content'] = 'Ubah Pembayaran';
  $data['list_project'] = $this->getListProject();
  echo Modules::run('template', $data);
 }

 public function getDetailDataPembayaran($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'p.nama_project',
                  'sp.status', 'c.nama as nama_pembeli', 'c.email',
                  'c.no_hp', 'c.alamat', 'p.harga',
                  'cp.nama as nama_company', 'cp.email as email_company',
                  'cp.no_hp as no_hp_company', 'cp.alamat as alamat_company'),
              'join' => array(
                  array('project p', 'k.project = p.id'),
                  array('(select max(id) as id, pembayaran from status_pembayaran group by pembayaran) spp', 'spp.pembayaran = k.id'),
                  array('status_pembayaran sp', 'spp.id = sp.id'),
                  array('customer c', 'p.customer = c.id'),
                  array('company cp', 'c.company = cp.id'),
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function bayar($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['pembayaran'] = $id;
   $post['status'] = 'LUNAS';
   Modules::run('database/_insert', "status_pembayaran", $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function detail($id) {
  $data = $this->getDetailDataPembayaran($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembayaran";
  $data['title_content'] = "Detail Pembayaran";
  echo Modules::run('template', $data);
 }

 public function getPostData($value) {
  $data['no_pembayaran'] = Modules::run('no_generator/generateNoPembayaran');
  $data['project'] = $value->project;
  $data['total_bayar'] = $value->total_bayar;
  $data['tanggal_bayar'] = $value->tanggal_bayar;
  $data['keterangan'] = $value->keterangan;

  return $data;
 }

 public function getPostPembayaran($value) {
  $data['validity'] = $value->validity;
  $data['issue_date'] = $value->issue_date;
  $data['due_date'] = $value->due_date;
  $data['remember_date'] = $value->remember_date;
  $data['conducted_by'] = $value->conducted_by;
  $data['status'] = $value->status;
  $data['manufacture'] = $value->manufacture;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');

  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
    $status_bayar['pembayaran'] = $id;
    $status_bayar['status'] = 'PENDING';
    Modules::run('database/_insert', "status_pembayaran", $status_bayar);
   } else {
    unset($post['no_pembayaran']);
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pembayaran";
  $data['title_content'] = 'Data Pembayaran';
  $content = $this->getDataPembayaran($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListPembayaran($project) {
  $data = Modules::run("database/get", array(
              'table' => $this->getTableName() . ' p',
              "field" => array('p.*'),
              'join' => array(
                  array('project pj', 'p.project = pj.id'),
                  array('(select max(id) as id, pembayaran from status_pembayaran group by pembayaran) spp', 'spp.pembayaran = p.id'),
                  array('status_pembayaran sp', 'spp.id = sp.id'),
              ),
              'where' => array('pj.id' => $project, 'p.deleted' => 0, 'sp.status' => 'LUNAS'),
              'orderby' => "p.id asc"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function cetak($id = '') {
  $mpdf = Modules::run('mpdf/getInitPdf');
  $data = $this->getDetailDataPembayaran($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembayaran";
  $data['title_content'] = 'Detail Pembayaran';
  $data['list_bayar'] = $this->getListPembayaran($data['project']);
  //  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data['no_pembayaran'] . '.pdf', 'I');
 }

}

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title><?php echo $no_pembayaran; ?></title>?>" />

        <style>
            body {
                font-family: "Helvetica", sans-serif;
                font-size: 11px;
            }
            table {
                width: 100%;
            }
            .text-center {
                text-align: center;
            }
            .text-right {
                text-align: right;
            }
            .font-bold {
                font-weight: bold;
            }
            .mb-32px {
                margin-bottom: 32px;
            }
            .mr-8px {
                margin-right: 8px;
            }
            .ml-8px {
                margin-left: 8px;
            }
            .table-logo {
                width: 100%;
            }
            table.table-logo > tbody > tr > td {
                padding: 16px;
            }
            table.table-logo td {
                vertical-align: top;
            }
            .table-item {
                width: 100%;
                margin-top: 16px;
                border-collapse: collapse;
                font-size: 8px;
            }
            table.table-item th,  table.table-item td {
                border: 1px solid #333;
                padding: 4px 8px;
                border-collapse: collapse;
                vertical-align: top;
            }
            .media {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-align: start;
                align-items: flex-start;
            }
            .media-body {
                -ms-flex: 1;
                flex: 1;
            }

            .none-border{
                border:none;
            }
        </style>
    </head>
    <body>
        <table class="table-item">   
            <tbody>
                <tr>
                    <td style="border:none;font-size: 16px;text-align:center;" colspan="2"><u><?php echo $nama_company ?></u></td>
                </tr>
                <tr>
                 <td style="border:none;font-size: 12px;text-align:center;" colspan="2"><?php echo $no_hp_company ?> / <?php echo $email_company ?></td>
                </tr>
                <tr>
                    <td style="border:none;font-size: 16px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border:none;font-size: 16px;">Pembayaran Kepada</td>
                </tr>
                <tr>
                    <td style="border:none;font-size: 16px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border:none;font-weight: bold;font-size: 14px;"><?php echo $nama_pembeli ?></td>
                    <td style="border:none;font-size: 14px;font-weight: bold;text-align: right;"><?php echo $no_pembayaran ?></td>
                </tr>
                <tr>  
                    <td style="border:none;font-size: 14px;"><?php echo $alamat ?></td>
                    <td style="border:none;font-size: 14px;text-align: right;"><?php echo 'Tgl Faktur, <b>' . $createddate.'</b>' ?></td>
                </tr>
                <tr>  
                    <td style="border:none;font-size: 14px;"><?php echo $no_hp ?></td>
                    <td style="border:none;font-size: 14px;text-align: right;"><?php echo 'Tgl Bayar, <b>' . $tanggal_bayar.'</b>' ?></td>
                </tr>
                <tr>  
                    <td style="border:none;font-size: 14px;"><?php echo $email ?></td>
                </tr>
            </tbody>
        </table>
        <br/>

        <hr/>
        <table class="table-item" style="width: 100%;margin-left: 24px;">   
            <tbody>
                <tr>
                    <td colspan="6" style="border:none;font-size: 14px;"><u><b>Daftar Item</b></u></td>
                </tr>
                <tr>
                    <th style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">No</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">Nama Project</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">Harga</th>
                </tr>
                <tr>
                    <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;"><?php echo '1' ?></td>
                    <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;"><?php echo $nama_project ?></td>
                    <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;font-weight: bold;"><?php echo 'Rp, ' . number_format($harga, 2) ?></td>
                </tr>
            </tbody>
        </table>
        <br/>

        <?php if (!empty($list_bayar)) { ?>
         <table class="table-item" style="width: 100%;margin-left: 24px;">   
             <tbody>
                 <tr>
                     <td colspan="6" style="border:none;font-size: 14px;"><u><b>Daftar Pembayaran</b></u></td>
                 </tr>
                 <tr>
                     <th style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">No</th>
                     <th style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">Keterangan</th>
                     <th style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">Total</th>
                 </tr>
                 <?php $no = 1; ?>
                 <?php $total = 0; ?>
                 <?php foreach ($list_bayar as $value) { ?>
                  <tr>
                      <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;"><?php echo $no++ ?></td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;"><?php echo $value['keterangan'] ?></td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;"><?php echo 'Rp, ' . number_format($value['total_bayar'], 2) ?></td>
                      <?php $total += $value['total_bayar'] ?>
                  </tr>
                 <?php } ?>
                  <tr>
                      <td colspan="2" style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;">Total</td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;font-size: 12px;font-weight: bold;"><?php echo 'Rp, ' . number_format($total, 2) ?></td>
                  </tr>
             </tbody>
         </table>
        <?php } ?>

        
        <table class="table-item">   
            <tbody>
                <tr>
                    <td style="border:none;font-size: 16px;">Catatan</td>
                    <td style="border:none;font-size: 16px;font-weight: bold;">Tagihan</td>
                    <td style="border:none;font-size: 16px;text-align: right;"><?php echo 'Rp, ' . number_format($total_bayar, 2) ?></td>
                </tr>
                <tr>
                    <td style="border:none;font-weight: bold;font-size: 14px;"><?php echo '*' . $keterangan ?></td>
                    <td style="border:none;font-weight: bold;font-size: 14px;color: green;">Dibayar</td>
                    <?php if ($status == 'PENDING') { ?>
                     <td style="border:none;font-weight: bold;font-size: 14px;color: green;text-align: right;"><?php echo 'Rp, ' . number_format(0, 2) ?></td>
                    <?php } else { ?>
                     <td style="border:none;font-weight: bold;font-size: 14px;color: green;text-align: right;">-<?php echo 'Rp, ' . number_format($total_bayar, 2) ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td style="border:none;font-weight: bold;font-size: 14px;">&nbsp;</td>
                    <td style="border:none;font-weight: bold;font-size: 14px;">Total</td>
                    <?php if ($status == 'PENDING') { ?>
                     <td style="border:none;font-weight: bold;font-size: 14px;text-align: right;"><?php echo '-Rp, ' . number_format($total_bayar, 2) ?></td>
                    <?php } else { ?>
                     <td style="border:none;font-weight: bold;font-size: 14px;text-align: right;"><?php echo 'Rp, ' . number_format(0, 2) ?></td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
        <br/>
    </body>
</html>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Pembayaran <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Project</label>
           <div class="col-md-9">
            <select id='project' class="form-control required" error='Project'>
             <?php if (!empty($list_project)) { ?>
              <?php foreach ($list_project as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($project)) { ?>
                <?php $selected = $produk == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_project'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>

         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Total Pembayaran</label>
           <div class="col-md-9">
            <input type="text" id='total_bayar' maxlength="30" class="form-control text-right required" error='Harga Pembayaran' placeholder="Harga Pembayaran" value="<?php echo isset($total_bayar) ? $total_bayar : '0' ?>">
           </div>
          </div>
         </div>
        </div>       
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Tanggal Bayar</label>
           <div class="col-md-9">
            <input type="text" id='tanggal_bayar' maxlength="30" class="form-control text-right required" error='Harga Pembayaran' placeholder="Tanggal Bayar" value="<?php echo isset($tanggal_bayar) ? $tanggal_bayar : '' ?>">
           </div>
          </div>
         </div>

         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Keterangan</label>
           <div class="col-md-9">
            <textarea class="form-control" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
           </div>
          </div>
         </div>
        </div>       
       </div>

       <div class="row">
        <div class="col-md-12">
         <div class="row">
          <div class="col-md-offset-3 col-md-9 text-right">
           <button type="submit" class="btn btn-success" onclick="Pembayaran.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
           <button type="button" class="btn btn-default" onclick="Pembayaran.back()">Batal</button>
          </div>
         </div>
        </div>
        <div class="col-md-6"> </div>
       </div>
     </div>

     <br/>
     </form>
    </div>
   </div>
  </div>
 </div> 
</div>
<!--./row--> 
</div>

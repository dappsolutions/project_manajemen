<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"> <?php echo $title_content ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="#" class="form-horizontal">
                            <div class="form-body">
                                <h3 class="box-title">Detail Pembayaran <i class="fa fa-arrow-down"></i></h3>
                                <hr class="m-t-0 m-b-40">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                No Pembayaran
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $no_pembayaran ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Total Pembayaran
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo 'Rp, ' . number_format($total_bayar, 2) ?>
                                            </div>
                                        </div>         
                                    </div>
                                </div>
                                <br/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Pembeli
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $nama_pembeli ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Email / No Hp
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $email . ' / ' . $no_hp; ?>
                                            </div>
                                        </div>         
                                    </div>
                                </div>
                                <br/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Nama Project
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $nama_project ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Tanggal Bayar
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $tanggal_bayar; ?>
                                            </div>
                                        </div>         
                                    </div>
                                </div>
                                <br/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Keterangan
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $keterangan ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Status
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php if ($status == 'PENDING') { ?>
                                                 <label class="label label-warning font-10 text-white"><?php echo $status ?></label>
                                                 <br/>
                                                 <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                                                  <br/>
                                                  <i class="mdi mdi-check mdi-18px"></i><label class="label label-success font-10 text-white hover" onclick="Pembayaran.bayar()">Tandai Sudah Dibayar</label>
                                                 <?php } ?>
                                                <?php } else { ?>
                                                 <label class="label label-success font-10 text-white"><?php echo $status ?></label>
                                                <?php } ?>
                                            </div>
                                        </div>         
                                    </div>
                                </div>
                                <br/>
                            </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9 text-right">
                                                <button type="button" class="btn btn-danger" onclick="Pembayaran.cetak()">Cetak</button>
                                                <button type="button" class="btn btn-default" onclick="Pembayaran.back()">Kembali</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--./row--> 
</div>

<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="Customer.add()">Tambah</button>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="Customer.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">Nama</th>
         <th class="font-12">Email</th>
         <th class="font-12">No Hp</th>
         <th class="font-12">Alamat</th>
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['nama'] ?></td>
           <td class='font-12'><?php echo $value['email'] ?></td>
           <td class='font-12'><?php echo $value['no_hp'] ?></td>
           <td class='font-12'><?php echo $value['alamat'] ?></td>
           <td class="text-center">
            <i class="mdi mdi-grease-pencil mdi-18px hover text-warning" onclick="Customer.ubah('<?php echo $value['id'] ?>')"></i>
            &nbsp;
            <i class="mdi mdi-clipboard-outline mdi-18px hover text-success" onclick="Customer.detail('<?php echo $value['id'] ?>')"></i>
            &nbsp;
            <i class="mdi mdi-delete mdi-18px hover text-danger" onclick="Customer.delete('<?php echo $value['id'] ?>')"></i>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="13">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>     
    </div>
   </div>    
   <br/>

   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
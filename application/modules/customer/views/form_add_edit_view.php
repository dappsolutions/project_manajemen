<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Customer <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama</label>
           <div class="col-md-9">
            <input type="text" id='nama' class="form-control required" error='Nama Customer' placeholder="Nama Customer" value="<?php echo isset($nama) ? $nama : '' ?>">
           </div>
          </div>
         </div>

         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Email</label>
           <div class="col-md-9">
            <input type="email" id='email' class="form-control required" error='Nama Email' placeholder="Email" value="<?php echo isset($email) ? $email : '' ?>">
           </div>
          </div>
         </div>
        </div>       

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">No Hp</label>
           <div class="col-md-9">
            <input type="text" id='no_hp' class="form-control required" error='No hp' placeholder="ex: +628674xxx" value="<?php echo isset($no_hp) ? $no_hp : '' ?>">
           </div>
          </div>
         </div>

         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Alamat</label>
           <div class="col-md-9">
            <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
           </div>
          </div>
         </div>
        </div>       
       </div>

       <div class="row">
        <div class="col-md-12">
         <div class="row">
          <div class="col-md-offset-3 col-md-9 text-right">
           <button type="submit" class="btn btn-success" onclick="Customer.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
           <button type="button" class="btn btn-default" onclick="Customer.back()">Batal</button>
          </div>
         </div>
        </div>
        <div class="col-md-6"> </div>
       </div>
     </div>

     <br/>
     </form>
    </div>
   </div>
  </div>
 </div> 
</div>
<!--./row--> 
</div>

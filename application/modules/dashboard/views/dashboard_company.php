<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <div class="white-box text-center bg-warning">
                    <h3 class="text-white"><?php echo $total_project ?></h3>
                    <h4 class="text-white">PROJECT</h4>
                </div>
            </div>
            <div class="col-md-3">
                <div class="white-box text-center bg-primary">
                    <h3 class="text-white"><?php echo $total_project ?></h3>
                    <h4 class="text-white">SERVIS</h4>
                </div>
            </div>

            <div class="col-md-3">
                <div class="white-box text-center bg-success">
                    <h3 class="text-white">Rp, <?php echo number_format($total_bayar['dibayar']) ?></h3>
                    <h4 class="text-white">AMOUNT PAID</h4>
                </div>
            </div>

            <div class="col-md-3">
                <div class="white-box text-center bg-danger">
                    <h3 class="text-white">Rp, <?php echo number_format($total_bayar['belum_bayar']) ?></h3>
                    <h4 class="text-white">AMOUNT UNPAID</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='row'>
    <div class='col-md-12'>
        <div class="white-box">
            <div class="row">
                <div class="col-md-12">
                    <h4> Daftar Project Outstanding</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table color-bordered-table primary-bordered-table">
                            <thead>
                                <tr class="">
                                    <th class="font-12">No</th>
                                    <th class="font-12">No Project</th>
                                    <th class="font-12">Nama Project</th>
                                    <th class="font-12">Keterangan</th>
                                    <th class="font-12">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($project)) { ?>
                                 <?php $no = 1; ?>
                                 <?php foreach ($project as $value) { ?>
                                  <tr>
                                      <td class='font-12'><?php echo $no++ ?></td>
                                      <td class='font-12'><?php echo $value['no_project'] ?></td>
                                      <td class='font-12'><?php echo $value['nama_project'] ?></td>
                                      <td class='font-12'><?php echo $value['keterangan'] ?></td>
                                      <td class='font-12'><label class="label label-warning"><?php echo $value['status'] ?></label></td>
                                  </tr>
                                 <?php } ?>
                                <?php } else { ?>
                                 <tr>
                                     <td class="text-center font-12" colspan="10">Tidak Ada Data Ditemukan</td>
                                 </tr>
                                <?php } ?>         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
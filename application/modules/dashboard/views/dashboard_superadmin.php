<div class='row'>
    <div class='col-md-12'>
        <div class="white-box">
            <div class="row">
                <div class="col-md-12">
                    <h4> Daftar Perusahaan Aktif</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table color-bordered-table primary-bordered-table">
                            <thead>
                                <tr class="">
                                    <th class="font-12">No</th>
                                    <th class="font-12">Nama Persahaan</th>
                                    <th class="font-12">Jumlah Customer</th>
                                    <th class="font-12">Alamat Perusahaan</th>
                                    <th class="font-12">Pendapatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($perusahaan)) { ?>
                                 <?php $no = 1; ?>
                                 <?php $temp_data = "" ?>
                                 <?php $total_pendapatan = 0 ?>
                                 <?php foreach ($perusahaan as $value) { ?>
                                  <tr>
                                      <?php if ($temp_data != $value['nama'] . $value['jumlah_customer']) { ?>
                                       <td class='font-12'><?php echo $no++ ?></td>
                                       <td class='font-12'><?php echo $value['nama'] ?></td>
                                       <td class='font-12'><?php echo $value['jumlah_customer'] ?></td>
                                       <td class='font-12'><?php echo $value['alamat'] ?></td>
                                       <td class='font-12'><?php echo 'Rp, ' . number_format($value['total_bayar']) ?></td>
                                       <?php $temp_data = $value['nama'] . $value['jumlah_customer']; ?>
                                      <?php } else { ?>
                                       <td class='font-12'><?php echo '' ?></td>
                                       <td class='font-12'><?php echo '' ?></td>
                                       <td class='font-12'><?php echo '' ?></td>
                                       <td class='font-12'><?php echo '' ?></td>
                                       <td class='font-12'><?php echo 'Rp, ' . number_format($value['total_bayar']) ?><br/>
                                       </td>
                                      <?php } ?>
                                  </tr>
                                 <?php } ?>
                                <?php } else { ?>
                                 <tr>
                                     <td class="text-center font-12" colspan="10">Tidak Ada Data Ditemukan</td>
                                 </tr>
                                <?php } ?>         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
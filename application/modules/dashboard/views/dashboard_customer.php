<div class='row'>
    <div class='col-md-12'>
        <div class="white-box">
            <div class="row">
                <div class="col-md-12">
                    <h4> Daftar Project Outstanding</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table color-bordered-table primary-bordered-table">
                            <thead>
                                <tr class="">
                                    <th class="font-12">No</th>
                                    <th class="font-12">No Project</th>
                                    <th class="font-12">Nama Project</th>
                                    <th class="font-12">Keterangan</th>
                                    <th class="font-12">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($project)) { ?>
                                 <?php $no = 1; ?>
                                 <?php foreach ($project as $value) { ?>
                                  <tr>
                                      <td class='font-12'><?php echo $no++ ?></td>
                                      <td class='font-12'><?php echo $value['no_project'] ?></td>
                                      <td class='font-12'><?php echo $value['nama_project'] ?></td>
                                      <td class='font-12'><?php echo $value['keterangan'] ?></td>
                                      <td class='font-12'><label class="label label-warning"><?php echo $value['status'] ?></label></td>
                                  </tr>
                                 <?php } ?>
                                <?php } else { ?>
                                 <tr>
                                     <td class="text-center font-12" colspan="10">Tidak Ada Data Ditemukan</td>
                                 </tr>
                                <?php } ?>         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 


<div class='row'>
    <div class='col-md-12'>
        <div class="white-box">
            <div class="row">
                <div class="col-md-12">
                    <h4> Daftar Tagihan</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table color-bordered-table danger-bordered-table">
                            <thead>
                                <tr class="">
                                    <th class="font-12">No</th>
                                    <th class="font-12">No Project</th>
                                    <th class="font-12">No Invoice</th>
                                    <th class="font-12">Nama Project</th>
                                    <th class="font-12">Keterangan</th>
                                    <th class="font-12">Total Tagihan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($tagihan)) { ?>
                                 <?php $no = 1; ?>
                                 <?php foreach ($tagihan as $value) { ?>
                                  <tr>
                                      <td class='font-12'><?php echo $no++ ?></td>
                                      <td class='font-12'><?php echo $value['no_project'] ?></td>
                                      <td class='font-12'><?php echo $value['no_pembayaran'] ?></td>
                                      <td class='font-12'><?php echo $value['nama_project'] ?></td>
                                      <td class='font-12'><?php echo $value['keterangan'] ?></td>
                                      <td class='font-12'><?php echo 'Rp, '. number_format($value['total_bayar']) ?></td>
                                  </tr>
                                 <?php } ?>
                                <?php } else { ?>
                                 <tr>
                                     <td class="text-center font-12" colspan="10">Tidak Ada Tagihan</td>
                                 </tr>
                                <?php } ?>         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class='row'>
    <div class='col-md-12'>
        <div class="white-box">
            <div class="row">
                <div class="col-md-12">
                    <h4> Daftar Progress Project</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table color-bordered-table warning-bordered-table">
                            <thead>
                                <tr class="">
                                    <th class="font-12">No</th>
                                    <th class="font-12">No Project</th>
                                    <th class="font-12">Nama Project</th>
                                    <th class="font-12">Keterangan</th>
                                    <th class="font-12">Tanggal Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($progress)) { ?>
                                 <?php $no = 1; ?>
                                 <?php foreach ($progress as $value) { ?>
                                  <tr>
                                      <td class='font-12'><?php echo $no++ ?></td>
                                      <td class='font-12'><?php echo $value['no_project'] ?></td>
                                      <td class='font-12'><?php echo $value['nama_project'] ?></td>
                                      <td class='font-12'><?php echo $value['keterangan'] ?></td>
                                      <td class='font-12'><?php echo date('d F Y', strtotime($value['tanggal'])) ?></td>
                                  </tr>
                                 <?php } ?>
                                <?php } else { ?>
                                 <tr>
                                     <td class="text-center font-12" colspan="10">Tidak Ada Tagihan</td>
                                 </tr>
                                <?php } ?>         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
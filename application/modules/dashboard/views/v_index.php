<input type='hidden' name='' id='hak_akses' class='form-control' value='<?php echo $this->session->userdata('hak_akses') ?>'/>
<link href="<?php echo base_url() ?>assets/plugins/components/vegas/vegas.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/plugins/components/vegas/vegas.min.js"></script>
<div class="container-fluid">
    <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
     <?php echo $this->load->view('dashboard_superadmin', $data, true) ?>
    <?php } ?>
    <?php if ($this->session->userdata('hak_akses') == 'company') { ?>
     <?php echo $this->load->view('dashboard_company', $data, true) ?> 
    <?php } ?>
    <?php if ($this->session->userdata('hak_akses') == 'customer') { ?>
     <?php echo $this->load->view('dashboard_customer', $data, true) ?> 
    <?php } ?>
</div>

<script>
 $("#header_dashboard").vegas({
     slides: [
         {src: '<?php echo base_url() . 'assets/images/background/profile-bg.jpg' ?>'}
     ],
 });
</script>
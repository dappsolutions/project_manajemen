<?php

class Dashboard extends MX_Controller {

 public $hak_akses;
 public $company_id;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  switch ($this->hak_akses) {
   case "superadmin":
    $data['perusahaan'] = $this->getDataPerusahaanAktif();
    break;
   case "customer":
    $data['project'] = $this->getDataProjectOnGoing();
    $data['tagihan'] = $this->getDataProjectTagihan();
    $data['progress'] = $this->getDataProjectProgress();
    break;
   case "company":
    $data['project'] = $this->getDataProjectOnGoing();
    $data['total_project'] = $this->getTotalProject();
    $data['total_bayar'] = $this->getDataTotalBayar();
    break;

   default:
    break;
  }
  echo Modules::run('template', $data);
 }

 public function getDataProjectProgress() {
  $customer = $this->session->userdata('customer_id');
  $data = Modules::run("database/get", array(
              'table' => "project_progress pp",
              "field" => array('pp.*', 'p.no_project', 'p.nama_project'),
              'join' => array(
                  array('project p', 'p.id = pp.project')
              ),
              'where' => array('pp.deleted'=> 0, 'p.customer'=> $customer),
              'orderby' => "pp.tanggal desc" 
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function getDataProjectTagihan() {
  $data = Modules::run("database/get", array(
              'table' => "pembayaran p",
              "field" => array('p.*', 'sp.status as status_bayar', 'pj.no_project', 'pj.nama_project'),
              'join' => array(
                  array('project pj', 'pj.id = p.project'),
                  array('(select max(id) as id, pembayaran from status_pembayaran where deleted = 0 group by pembayaran) spp', 'spp.pembayaran = p.id'),
                  array('status_pembayaran sp', 'sp.id = spp.id')
              ),
              'where' => array('p.deleted'=> 0, 
                  'pj.customer'=> $this->session->userdata('customer_id'), 'sp.status'=> 'PENDING'),
              'orderby' => "p.createddate desc" 
  ));
  
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function getDataPerusahaanAktif() {
  $data = Modules::run("database/get", array(
              'table' => "company c",
              "field" => array('c.*', 'cpp.jumlah_customer', 'spp.total_bayar'),
              'join' => array(
                  array('(select count(id) as jumlah_customer, company from customer where deleted = 0 group by company) cpp', 'cpp.company = c.id'),
                  array('kategori_project kp', 'kp.company = c.id'),
                  array('project pj', 'pj.kategori_project = kp.id'),
                  array('(select sum(total_bayar) as total_bayar, id, project, deleted from pembayaran where deleted = 0 group by project) spp', 'pj.id = spp.project'),
                  array('(select max(id) as id, pembayaran from status_pembayaran where deleted = 0 group by pembayaran) stp', 'stp.pembayaran = spp.id'),
                  array('status_pembayaran sp', "sp.id = stp.id and sp.status = 'LUNAS'")
              ),
              'where' => array('c.deleted' => 0)
  ));

//  echo '<pre>';
//  print_r($data->result_array());
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataTotalBayar() {
  switch ($this->hak_akses) {
   case 'superadmin':
    $where = "pj.deleted = 0";
    break;
   case 'company':
    $where = "pj.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;

   default:
    break;
  }

  $data = Modules::run("database/get", array(
              'table' => "project pj",
              "field" => array('pj.*', 'spp.total_bayar'),
              'join' => array(
                  array('kategori_project kp', 'pj.kategori_project = kp.id'),
                  array('(select sum(total_bayar) as total_bayar, id, project, deleted from pembayaran where deleted = 0 group by project) spp', 'pj.id = spp.project'),
                  array('(select max(id) as id, pembayaran from status_pembayaran where deleted = 0 group by pembayaran) stp', 'stp.pembayaran = spp.id'),
                  array('status_pembayaran sp', "sp.id = stp.id and sp.status = 'LUNAS'")
              ),
              'where' => $where,
  ));

  $total_dibayar = 0;
  $total_belum = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $harga = $value['harga'];
    $total_dibayar += $value['total_bayar'];
    $margin = $harga - $value['total_bayar'];
    $total_belum += $margin;
   }
  }

  return array(
      'dibayar' => $total_dibayar,
      'belum_bayar' => $total_belum
  );
 }

 public function getTotalProject() {
  switch ($this->hak_akses) {
   case 'superadmin':
    $where = "pj.deleted = 0";
    break;
   case 'company':
    $where = "pj.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;

   default:
    break;
  }

  $total = Modules::run("database/count_all", array(
              'table' => "project pj",
              "field" => array('pj.*'),
              'join' => array(
                  array('kategori_project kp', 'pj.kategori_project = kp.id')
              ),
              'where' => $where
  ));

  return $total;
 }

 public function getDataProjectOnGoing() {
  $date = date('Y-m-d');

  switch ($this->hak_akses) {
   case "superadmin":
    $join = array(
        array('(select max(id) as id, project from project_status group by project) pss', 'pss.project = pj.id'),
        array('project_status ps', 'pss.id = ps.id')
    );
    $where = "pj.deleted = 0";
    break;
   case "company":
    $join = array(
        array('(select max(id) as id, project from project_status group by project) pss', 'pss.project = pj.id'),
        array('project_status ps', 'pss.id = ps.id'),
        array('produk p', 'p.id = pj.produk'),
    );
    $where = "pj.deleted = 0 and p.company = '" . $this->company_id . "' and ps.status = 'DRAFT'";
    break;
   case "customer":
    $join = array(
        array('(select max(id) as id, project from project_status group by project) pss', 'pss.project = pj.id'),
        array('project_status ps', 'pss.id = ps.id'),
        array('produk p', 'p.id = pj.produk'),
    );
    $where = "pj.deleted = 0 and pj.customer = '" . $this->session->userdata('customer_id') . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => 'project pj',
              'field' => array('pj.*', 'ps.status', 'ps.keterangan'),
              'join' => $join,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

}

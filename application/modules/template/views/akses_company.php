<?php if ($this->session->userdata('hak_akses') == 'company') { ?>
 <li>
     <a class="waves-effect active" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
 </li>
 <li>
     <a class="waves-effect" href="<?php echo base_url() . 'customer' ?>" aria-expanded="false"><i class="icon-people fa-fw"></i> <span class="hide-menu"> Customer </span></a>
 </li>

 <li>
     <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-credit-card fa-fw"></i> <span class="hide-menu"> Bank </span></a>
     <ul aria-expanded="false" class="collapse">     
         <li> <a href="<?php echo base_url() . 'bank' ?>"><?php echo 'Data Bank' ?></a> </li>
         <li> <a href="<?php echo base_url() . 'akunbank' ?>"><?php echo 'Akun Bank' ?></a> </li>
     </ul>
 </li> 

 <li>
     <a class="waves-effect" href="<?php echo base_url() . 'produk' ?>" aria-expanded="false"><i class="icon-grid fa-fw"></i> <span class="hide-menu"> Produk </span></a>
 </li>

 <li>
     <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-cloud-upload fa-fw"></i> <span class="hide-menu"> Project </span></a>
     <ul aria-expanded="false" class="collapse">     
         <li> <a href="<?php echo base_url() . 'kategoriproject' ?>"><?php echo 'Kategori Project' ?></a> </li>
         <li> <a href="<?php echo base_url() . 'project' ?>"><?php echo 'Project' ?></a> </li>
         <li> <a href="<?php echo base_url() . 'projectprogress' ?>"><?php echo 'Project Progress' ?></a> </li>
     </ul>
 </li> 

 <li>
     <a class="waves-effect" href="<?php echo base_url() . 'pembayaran' ?>" aria-expanded="false"><i class="icon-wallet fa-fw"></i> <span class="hide-menu"> Pembayaran </span></a>
 </li>
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
 <li>
     <a class="waves-effect active" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
 </li>
 <li>
     <a class="waves-effect" href="<?php echo base_url() . 'company' ?>" aria-expanded="false"><i class="icon-user-follow fa-fw"></i> <span class="hide-menu"> Perusahaan </span></a>
 </li>
<?php } ?>
<!-- ===== Left-Sidebar ===== -->
<aside class="sidebar">
 <div class="scroll-sidebar">
  <div class="user-profile">
   <div class="dropdown user-pro-body">
    <div class="profile-image">
     <img src="<?php echo base_url() ?>assets/images/users/images.png" alt="profile image" class="img-circle">
     <a href="javascript:void(0);" class="dropdown-toggle u-dropdown text-blue" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
      <span class="badge badge-danger">
       <i class="fa fa-angle-down"></i>
      </span>
     </a>
     <ul class="dropdown-menu animated flipInY">
      <li role="separator" class="divider"></li>
      <li><a href="javascript:void(0);" onclick="Template.changePassword(this, event)"><i class="fa fa-cog"></i> Change Password</a></li>
      <li role="separator" class="divider"></li>
      <li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-power-off"></i> Logout</a></li>
     </ul>
    </div>
    <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> <?php echo strtoupper($this->session->userdata('username')) ?></a></p>
   </div>
  </div>
  <nav class="sidebar-nav">
   <ul id="side-menu">
    <?php echo $this->load->view("akses_admin") ?>
    <?php echo $this->load->view("akses_company") ?>
    <?php echo $this->load->view("akses_customer") ?>
    <li>
     <a class="waves-effect" href="<?php echo base_url() . 'login/sign_out' ?>" aria-expanded="false"><i class="icon-power fa-fw"></i> <span class="hide-menu"> Log out </span></a>
    </li>
   </ul>
  </nav>
 </div>
</aside>
<!-- ===== Left-Sidebar-End ===== -->
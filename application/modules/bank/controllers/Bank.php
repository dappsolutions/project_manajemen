<?php

class Bank extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $hak_akses;
 public $company_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'bank';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/bank.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'bank';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Bank";
  $data['title_content'] = 'Data Bank';
  $content = $this->getDataBank();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataBank($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pj.nama_bank', $keyword),
   );
  }
  
  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pj.deleted = 0";
    break;
   case "company":
    $where = "pj.deleted = 0 and cp.id = '" . $this->company_id . "'";
    break;

   default:
    break;
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*'),
              'join' => array(
                  array('company cp', 'cp.id = pj.company', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where
  ));

  return $total;
 }

 public function getDataBank($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pj.nama_bank', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pj.deleted = 0";
    break;
   case "company":
    $where = "pj.deleted = 0 and cp.id = '" . $this->company_id . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*'),
              'join' => array(
                  array('company cp', 'cp.id = pj.company', 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataBank($keyword)
  );
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Bank";
  $data['title_content'] = 'Tambah Bank';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataBank($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Bank";
  $data['title_content'] = 'Ubah Bank';
  echo Modules::run('template', $data);
 }

 public function getDetailDataBank($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'where' => "k.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataBank($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Bank";
  $data['title_content'] = "Detail Bank";
  echo Modules::run('template', $data);
 }

 public function getPostData($value) {
  $data['nama_bank'] = $value->nama_bank;
  $data['company'] = $this->company_id;
  return $data;
 }

 public function getPostBank($value) {
  $data['validity'] = $value->validity;
  $data['issue_date'] = $value->issue_date;
  $data['due_date'] = $value->due_date;
  $data['remember_date'] = $value->remember_date;
  $data['conducted_by'] = $value->conducted_by;
  $data['status'] = $value->status;
  $data['manufacture'] = $value->manufacture;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Bank";
  $data['title_content'] = 'Data Bank';
  $content = $this->getDataBank($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

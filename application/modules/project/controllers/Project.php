<?php

class Project extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $hak_akses;
 public $company_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->company_id = $this->session->userdata('company_id');
 }

 public function getModuleName() {
  return 'project';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/project.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'project';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Project";
  $data['title_content'] = 'Data Project';
  $content = $this->getDataProject();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProject($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pj.nama_project', $keyword),
       array('pj.no_project', $keyword),
       array('ps.status', $keyword),
       array('c.nama', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pj.deleted = 0";
    break;
   case "company":
    $where = "pj.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "pj.deleted = 0 and pj.customer = '" . $this->session->userdata('customer_id') . "'";
    break;
   default:
    break;
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*', 'ps.status', 'ps.keterangan', 'c.nama as pembeli'),
              'join' => array(
                  array('(select max(id) as id, project from project_status group by project) pss', 'pss.project = pj.id'),
                  array('project_status ps', 'pss.id = ps.id'),
                  array('customer c', 'pj.customer = c.id', 'left'),
                  array('kategori_project kp', 'kp.id = pj.kategori_project'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where
  ));

  return $total;
 }

 public function getDataProject($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pj.nama_project', $keyword),
       array('pj.no_project', $keyword),
       array('ps.status', $keyword),
       array('c.nama', $keyword),
   );
  }

  switch ($this->hak_akses) {
   case "superadmin":
    $where = "pj.deleted = 0";
    break;
   case "company":
    $where = "pj.deleted = 0 and kp.company = '" . $this->company_id . "'";
    break;
   case "customer":
    $where = "pj.deleted = 0 and pj.customer = '" . $this->session->userdata('customer_id') . "'";
    break;

   default:
    break;
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*', 'ps.status', 'ps.keterangan', 'c.nama as pembeli'),
              'join' => array(
                  array('(select max(id) as id, project from project_status group by project) pss', 'pss.project = pj.id'),
                  array('project_status ps', 'pss.id = ps.id'),
                  array('customer c', 'pj.customer = c.id', 'left'),
                  array('kategori_project kp', 'kp.id = pj.kategori_project'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProject($keyword)
  );
 }

 public function getListKategoriProject() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_project kp',
              'field' => array('kp.*'),
              'where' => "kp.deleted = 0 and kp.company = '" . $this->company_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'produk p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.company = '" . $this->company_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   array_push($result, array(
       'id'=> '',
       'nama_produk'=> 'Pilih Produk',
   ));
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListCustomer() {
  $data = Modules::run('database/get', array(
              'table' => 'customer c',
              'field' => array('c.*'),
              'where' => "c.deleted = 0 and c.company = '" . $this->company_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Project";
  $data['title_content'] = 'Tambah Project';
  $data['list_kategori'] = $this->getListKategoriProject();
  $data['list_produk'] = $this->getListProduk();
  $data['list_customer'] = $this->getListCustomer();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProject($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Project";
  $data['title_content'] = 'Ubah Project';
  $data['list_kategori'] = $this->getListKategoriProject();
  $data['list_produk'] = $this->getListProduk();
  $data['list_customer'] = $this->getListCustomer();
  echo Modules::run('template', $data);
 }

 public function getDetailDataProject($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pj',
              'field' => array('pj.*', 'p.nama_produk', 'kp.kategori', 'c.nama as pembeli', 'ps.status'),
              'join' => array(
                  array('produk p', 'pj.produk = p.id'),
                  array('kategori_project kp', 'pj.kategori_project = kp.id'),
                  array('(select max(id) as id, project from project_status group by project) pss', 'pss.project = pj.id'),
                  array('project_status ps', 'pss.id = ps.id'),
                  array('customer c', 'pj.customer = c.id', 'left')
              ),
              'where' => "pj.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataProject($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Project";
  $data['title_content'] = "Detail Project";
  echo Modules::run('template', $data);
 }

 public function getPostAlatName($value) {
  $data['nama_alat'] = $value->equipment;
  $data['kode_alat'] = $value->identity;
  return $data;
 }

 public function getPostData($value) {
  $data['no_project'] = Modules::run("no_generator/generateNoProject");
  $data['customer'] = $value->pembeli;
  $data['nama_project'] = $value->nama_project;
  $data['produk'] = $value->produk;
  $data['tanggal_order'] = $value->tanggal;
  $data['harga'] = $value->harga;
  $data['kategori_project'] = $value->kategori_project;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostData($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);

    $pstatus['status'] = "DRAFT";
    $pstatus['project'] = $id;
    Modules::run('database/_insert', "project_status", $pstatus);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Project";
  $data['title_content'] = 'Data Project';
  $content = $this->getDataProject($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function tandaiSelesai() {
  $id = $this->input->post('id');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $pstatus['status'] = "COMPLETE";
   $pstatus['project'] = $id;
   Modules::run('database/_insert', "project_status", $pstatus);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}

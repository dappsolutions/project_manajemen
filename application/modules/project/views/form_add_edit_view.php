<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Project <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Pembeli</label>
           <div class="col-md-9">
            <select id='pembeli' class="form-control required" error='Pembeli'>
             <?php if (!empty($list_customer)) { ?>
              <?php foreach ($list_customer as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($customer)) { ?>
                <?php $selected = $customer == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Nama Project</label>
           <div class="col-md-9">
            <input type="text" id='nama_project' class="form-control required" placeholder="Nama Project" error='Nama Project' value="<?php echo isset($nama_project) ? $nama_project : '' ?>">
           </div>
          </div>
         </div>
         <!--/span-->
        </div>       
       </div>

       <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Produk</label>
          <div class="col-md-9">
           <select id='produk' class="form-control required" error='Produk'>
            <?php if (!empty($list_produk)) { ?>
             <?php foreach ($list_produk as $value) { ?>
              <?php $selected = '' ?>
              <?php if (isset($produk)) { ?>
               <?php $selected = $produk == $value['id'] ? 'selected' : '' ?>
              <?php } ?>
              <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_produk'] ?></option>
             <?php } ?>
            <?php } ?>
           </select>
          </div>
         </div>
        </div>

        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Tanggal Order</label>
          <div class="col-md-9">
           <input type="text" id='tanggal' class="form-control required" placeholder="Tanggal Order" error='Tanggal Order' value="<?php echo isset($tanggal_order) ? $tanggal_order : '' ?>">
          </div>
         </div>
        </div>
       </div>

       <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Harga</label>
          <div class="col-md-9">
           <input type="text" id='harga' class="form-control required text-right" placeholder="Harga" error='Harga' value="<?php echo isset($harga) ? $harga : '0' ?>">
          </div>
         </div>
        </div>

        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Kategori Project</label>
          <div class="col-md-9">
           <select id='kategori_project' class="form-control required" error='Kategori Project'>
            <?php if (!empty($list_kategori)) { ?>
             <?php foreach ($list_kategori as $value) { ?>
              <?php $selected = '' ?>
              <?php if (isset($kategori_project)) { ?>
               <?php $selected = $kategori_project == $value['id'] ? 'selected' : '' ?>
              <?php } ?>
              <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
             <?php } ?>
            <?php } ?>
           </select>
          </div>
         </div>
        </div>         
       </div>
     </div>

     <br/>
     </form>
    </div>
   </div>
  </div>
 </div>

 <div class="row">
  <div class="col-md-12">
   <div class="row">
    <div class="col-md-offset-3 col-md-9 text-right">
     <button type="submit" class="btn btn-success" onclick="Project.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
     <button type="button" class="btn btn-default" onclick="Project.back()">Batal</button>
    </div>
   </div>
  </div>
  <div class="col-md-6"> </div>
 </div>
</div>
<!--./row--> 
</div>

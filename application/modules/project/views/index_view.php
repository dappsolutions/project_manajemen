<div class='container-fluid'>

    <div class="white-box stat-widget">  
        <div class="card-body">
            <h4 class="card-title"><u><?php echo $title ?></u></h4>

            <div class='row'>
                <div class='col-md-2'>
                    <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                     <button id="" class="btn btn-block btn-warning" onclick="Project.add()">Tambah</button>
                    <?php } ?>
                </div>
                <div class='col-md-10'>
                    <input type='text' name='' id='' onkeyup="Project.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
                </div>     
            </div>        
            <div class='row'>
                <div class='col-md-12'>
                    <?php if (isset($keyword)) { ?>
                     <?php if ($keyword != '') { ?>
                      Cari Data : "<b><?php echo $keyword; ?></b>"
                     <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <br/>
            <hr/>

            <div class='row'>
                <div class='col-md-12'>
                    <div class="table-responsive">
                        <table class="table color-bordered-table primary-bordered-table">
                            <thead>
                                <tr class="">
                                    <th class="font-12">No</th>
                                    <th class="font-12">No Project</th>
                                    <th class="font-12">Nama Project</th>
                                    <th class="font-12">Harga</th>
                                    <th class="font-12">Status</th>
                                    <th class="font-12">Keterangan</th>
                                    <th class="text-center font-12">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($content)) { ?>
                                 <?php $no = 1; ?>
                                 <?php foreach ($content as $value) { ?>
                                  <tr>
                                      <td class='font-12'><?php echo $no++ ?></td>
                                      <td class='font-12'><?php echo $value['no_project'] ?></td>
                                      <td class='font-12'><?php echo $value['nama_project'] ?></td>
                                      <td class='font-12'><?php echo 'Rp, ' . number_format($value['harga'], 2) ?></td>
                                      <?php if ($value['status'] == 'DRAFT') { ?>
                                       <td class='font-12'><label class="label label-warning"><?php echo $value['status'] ?></label></td>
                                      <?php } else { ?>
                                       <td class='font-12'><label class="label label-success"><?php echo $value['status'] ?></label></td>
                                      <?php } ?>
                                      <td class='font-12'><?php echo $value['keterangan'] ?></td>
                                      <td class="text-center">
                                          <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                                           <i class="mdi mdi-grease-pencil mdi-18px hover text-warning" onclick="Project.ubah('<?php echo $value['id'] ?>')"></i>
                                           &nbsp;
                                          <?php } ?>
                                          <i class="mdi mdi-clipboard-outline mdi-18px hover text-success" onclick="Project.detail('<?php echo $value['id'] ?>')"></i>
                                          &nbsp;
                                          <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                                           <i class="mdi mdi-delete mdi-18px hover text-danger" onclick="Project.delete('<?php echo $value['id'] ?>')"></i>
                                           &nbsp;
                                          <?php } ?>
                                      </td>
                                  </tr>
                                 <?php } ?>
                                <?php } else { ?>
                                 <tr>
                                     <td class="text-center font-12" colspan="13">Tidak Ada Data Ditemukan</td>
                                 </tr>
                                <?php } ?>         
                            </tbody>
                        </table>
                    </div>     
                </div>
            </div>    
            <br/>

            <div class="row">
                <div class="col-md-12">
                    <div class="pagination">
                        <?php echo $pagination['links'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"> <?php echo $title_content ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="#" class="form-horizontal">
                            <div class="form-body">
                                <h3 class="box-title">Detail Project <i class="fa fa-arrow-down"></i></h3>
                                <hr class="m-t-0 m-b-40">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                No Project
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $no_project ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Pembeli
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $pembeli ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Nama Project
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $nama_produk ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Tanggal Order
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $tanggal_order ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>

                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Harga
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo 'Rp, ' . number_format($harga, 2) ?>
                                            </div>
                                        </div>         
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Kategori Project
                                            </div>
                                            <div class="col-md-9 text-primary text-left">
                                                <?php echo $kategori ?><br/><br/>
                                            </div>
                                        </div>         
                                    </div>
                                </div>
                                <br/>
                            </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9 text-right">
                                                <?php if ($status == "DRAFT") { ?>
                                                 <?php if ($this->session->userdata('hak_akses') != 'customer') { ?>
                                                  <button type="button" class="btn btn-success" onclick="Project.tandaiSelesai()">Selesai</button>
                                                 <?php } ?>
                                                <?php } ?>
                                                <button type="button" class="btn btn-default" onclick="Project.back()">Kembali</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--./row--> 
</div>

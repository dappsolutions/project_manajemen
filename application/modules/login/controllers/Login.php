<?php

class Login extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'login';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/controllers/login.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Login";
  $data['title_content'] = 'Login';
  echo $this->load->view('index', $data, true);
 }

 public function getDataUserDb($username, $password) {
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('u.*', 'p.hak_akses', 'cp.id as company_id', 
                  'cr.id as customer_id', 'crp.id as company_id_customer'),
              'join' => array(
                  array('priveledge p', 'p.id = u.priveledge'),
                  array('company cp', 'cp.user = u.id', 'left'),
                  array('customer cr', 'cr.user = u.id', 'left'),
                  array('company crp', 'cr.company = crp.id', 'left')
              ),
              'where' => array(
                  'u.username' => $username,
                  'u.password' => $password,
                  'u.deleted' => 0
              )
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getDataUser($username, $password) {
  $data = $this->getDataUserDb($username, $password);
  $result = array();
  if (!empty($data)) {
   $result = $data;
  }

  return $result;
 }

 public function sign_in() {
  $username = $this->input->post('username');
  $password = $this->input->post('password');

  $is_valid = false;
  try {
   $data = $this->getDataUser($username, $password);
   if (!empty($data)) {
    $is_valid = true;
   }
  } catch (Exception $exc) {
   $is_valid = false;
  }
  if ($is_valid) {
   $this->setSessionData($data);
  }
  echo json_encode(array('is_valid' => $is_valid));
 }

 public function setSessionData($data) {
  $session['user_id'] = $data['id'];
  $session['username'] = $data['username'];
  $session['hak_akses'] = $data['hak_akses'];
  $session['company_id'] = $data['company_id'];
  $session['customer_id'] = $data['customer_id'];
  switch ($data['hak_akses']) {
   case "company":
    $session['company_id'] = $data['company_id'];
    break;
   case "customer":
    $session['company_id'] = $data['company_id_customer'];
    $session['customer_id'] = $data['customer_id'];
    break;
   default:
    break;
  }
  $this->session->set_userdata($session);
 }

 public function sign_out() {
  $this->session->sess_destroy();
  redirect(base_url());
 }

}
